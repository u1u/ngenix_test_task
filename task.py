import os
import zipfile
import csv
import random
import multiprocessing
from multiprocessing import Queue
from shutil import copyfileobj
from io import StringIO, BytesIO
from uuid import uuid4
import xml.etree.ElementTree as ET

# some init
ZIP_NUM = 50
XML_PER_ZIP = 100
CPU_NUM = max(1, multiprocessing.cpu_count()-2)
# CPU_NUM = 1
XML_COUNT = 0
OBJ_COUNT = 0
random.seed()
if not os.path.exists('zips'):
    os.makedirs('zips')


def make_xml():

    '''Generate XML Tree'''

    global XML_COUNT, OBJ_COUNT
    XML_COUNT += 1

    root = ET.Element('root')

    attr_id = {
        'name': 'id',
        'value': str(uuid4())
    }

    attr_level = {
        'name': 'level',
        'value': str(random.randint(1, 100))
    }

    ET.SubElement(root, 'var', attrib=attr_id)
    ET.SubElement(root, 'var', attrib=attr_level)
    objects = ET.SubElement(root, 'objects')

    for _ in range(random.randint(1, 10)):
        OBJ_COUNT += 1
        attr_obj = {
            'name': str(uuid4())
        }
        ET.SubElement(objects, 'object', attrib=attr_obj)

    return root


def generate_archieves():

    '''Generate zip rchieves'''

    for i in range(ZIP_NUM):
        inmemory_xml_mas = []

        for _ in range(XML_PER_ZIP):
            output = StringIO()
            tree_str = ET.tostring(make_xml(), encoding="unicode")
            output.write(tree_str)
            inmemory_xml_mas.append(output)

        inmemory_io = BytesIO()

        with zipfile.ZipFile(inmemory_io, 'w') as inmemory_zip:  # first, in memory
            for j, file in enumerate(inmemory_xml_mas):
                inmemory_zip.writestr("{}.xml".format(j), file.getvalue())

        with open('zips/{}.zip'.format(i), 'wb') as ondisk_zip:
            inmemory_io.seek(0)
            copyfileobj(inmemory_io, ondisk_zip)  # then dump to disk


def solve(l, r, queue1, queue2):

    '''
    Walk over every zip building XML trees ans sending data to queues.
    First queue is for csv with id and xml level.
    Second is for csv with id and object names
    '''

    forest = []

    for i in range(l, r):
        with zipfile.ZipFile('zips/{}.zip'.format(i), 'r') as zip_file:
            for name in zip_file.namelist():
                forest.append(ET.parse(zip_file.open(name)).getroot())

    id_cur = -1
    for tree in forest:
        for childl1 in tree:

            if childl1.tag == 'objects':
                for childl2 in childl1:
                    queue2.put([id_cur, childl2.attrib['name']])

            if childl1.tag == 'var':
                if childl1.attrib['name'] == 'id':
                    id_cur = childl1.attrib['value']

                if childl1.attrib['name'] == 'level':
                    level_cur = childl1.attrib['value']
                    queue1.put([id_cur, level_cur])


def write_csv(name, queue, limit):

    '''Write data from queue to CSV file'''

    with open(name, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        data = []
        for i in range(limit):
            data.append(queue.get())
            if i % 10000 == 0:
                writer.writerows(data)
                data = []

        if len(data) != 0:
            writer.writerows(data)


def run_workers(func):

    files_per_core = ZIP_NUM // CPU_NUM
    rst = ZIP_NUM % CPU_NUM
    procs = []

    csv1_queue = Queue()
    csv2_queue = Queue()

    # run workers for reading from zip and sending data to queues
    for i in range(0, ZIP_NUM - rst, files_per_core):
        l = i
        r = i + files_per_core

        if r + rst == ZIP_NUM:
            r += rst

        p = multiprocessing.Process(target=func, args=(l, r, csv1_queue, csv2_queue))
        procs.append(p)
        p.start()

    # run workers for receiving data from queues and put it to CSV
    id_level_w = multiprocessing.Process(target=write_csv, args=('id_level.csv', csv1_queue, XML_COUNT))
    id_obj_w = multiprocessing.Process(target=write_csv, args=('id_obj.csv', csv2_queue, OBJ_COUNT))

    id_level_w.start()
    id_obj_w.start()

    for proc in procs:
        proc.join()

    id_level_w.join()
    id_obj_w.join()


if __name__ == "__main__":

    generate_archieves()
    run_workers(solve)
    print("OK")
